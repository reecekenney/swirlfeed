<?php include("includes/header.php");

if(isset($_GET['profile_username'])) {
	$username = $_GET['profile_username'];
	$user_details_query = mysqli_query($con, "SELECT * FROM users WHERE username='$username'");
	$profile_username = mysqli_fetch_array($user_details_query); //Details of user of profile page
	$message_obj = new Message($con, $userLoggedIn); //Message object for user logged in
}

//Pressed 'remove friend' button
if (@$_POST['removeFriend']) {
	$logged_in_user = new User($con, $user['username']);
	$logged_in_user->removeFriend($profile_username['username']);
}
//Pressed 'add friend' button 
if (@$_POST['addFriend']) {
	$logged_in_user = new User($con, $user['username']);
	$logged_in_user->sendRequest($profile_username['username']);
}
//Pressed 'Respond to request' button 
if (@$_POST['respondToRequest']) {
	header("Location: requests.php");
}

//Presses send button on message form
if (isset($_POST['post_message'])){
	//Check if message box has text in it
	if(isset($_POST['message_body'])){
		$body = mysql_real_escape_string($_POST['message_body']);
		$date = date("Y-m-d H:i:s");
		$message_obj->sendMessage($profile_username['username'], $body, $date);
	}

	$link = '#profileTabs a[href="#messages_div"]'; //Tab to be loaded
	echo "<script>
		  $(function () {
		    $('".$link."').tab('show');
		  })
		</script>";

}

?> 
<style type="text/css">
	.top_bar {
		margin-bottom: 0px;
	}
	.wrapper {
		margin-left: 0px;
		padding-left: 0px;
	}
</style>
<div class="profile_left">
	<img src="<?php echo $profile_username['profile_pic']; ?>">
	<div class="profile_left_right">
		<?php $num_friends = substr_count($profile_username['friend_array'], "," ) - 1; ?>
		<p><?php echo "Posts: ".$profile_username['num_posts']; ?></p>
		<p><?php echo "Bumps: ".$profile_username['num_bumps']; ?></p>
		<p><?php echo "Friends: ".$num_friends; ?></p>
	</div>

	<!-- Buttons for profile e.g. add friend, send message, poke -->
	<form action="<?php echo $profile_username["username"]?>" method="POST">
		<?php 

		//If user accountis closed, redirect.
		$profile_user = new User($con, $profile_username['username']);
		if($profile_user->isClosed() == "yes")
			header("Location: user_closed.php");

		$logged_in_user = new User($con, $user['username']); //User object for logged in user

		//If user is not on own profile
		if($user['username'] != $profile_username['username']){

			if ($logged_in_user->isFriend($profile_username['username'])) 
				//If users are friends, show remove friend button. 
				echo '<input type="submit" name="removeFriend" class="danger" value="Remove Friend"><br>';
			else if ($logged_in_user->didReceiveRequest($profile_username['username']))
				//If a request has been received from the profile user
				echo '<input type="submit" name="respondToRequest" class="warning" value="Respond to Request"><br>';
			else if ($logged_in_user->didSendRequest($profile_username['username']))
				//If a request has already been sent to the profile user (awaiting response)
				echo '<input type="submit" name="" class="default" value="Request Sent"><br>';
			else
				//If users are not friends, show add friend button
				echo '<input type="submit" name="addFriend" class="success" value="Add Friend"><br>';
		}
		//Post swirl button
		echo '<input type="submit" class="deep_blue" data-toggle="modal" data-target="#post_form" value="Post Something">';

		//If user is not on own profile
		if($user['username'] != $profile_username['username']){
			echo '<div class="profile_left_info">';
				//Mutual friends
				echo $logged_in_user->getMutualFriends($profile_username['username'])." friends in common";
			echo '</div>'; //End profile_left_info 
		}
		?>
		</form>
</div>


<div class="profile_main_column column">

	<!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist" id="profileTabs">
    <li role="presentation" class="active"><a href="#swirlfeed_div" aria-controls="swirlfeed_div" role="tab" data-toggle="tab">Swirlfeed</a></li>
    <li role="presentation"><a href="#about_div" aria-controls="about_div" role="tab" data-toggle="tab">About</a></li>
    <li role="presentation"><a href="#messages_div" aria-controls="messages_div" role="tab" data-toggle="tab">Message</a></li>
  </ul>

 	<div class="tab-content">
 		<!-- Swirlfeed div -->
 		<div role="tabpanel" class="tab-pane fade in active" id="swirlfeed_div">
			<div class="swirls_area"></div>
			<img id='loading' src='assets/images/icons/loading.gif'>
		</div>

		<!-- About div -->
		<div role="tabpanel" class="tab-pane fade" id="about_div">
			sdagagas
		</div>

		<!-- Messages div -->
		<div role="tabpanel" class="tab-pane fade" id="messages_div">
			<?php 
			$profile_user_obj = new User($con, $profile_username['username']);
			echo "<h4>You and <a href='".$profile_username['username']."'>" . $profile_user_obj->getFirstAndLastName() . "</a></h4><hr><br>"; 

			?>
			<div class="loaded_messages" id="x" style="height: 56%;">
				<?php echo $message_obj->getMessages($profile_username['username']); ?>
			</div>

			<div class="message_post">
				<form action="" method="POST">
					<textarea name="message_body" id="message_textarea" placeholder="Write your message..."></textarea>
					<input type="submit" name="post_message" class="info" id="message_submit" value="Send">
				</form>
			</div>

			<!-- Javascript to scroll messages to bottom -->
			<script>
				var objDiv = document.getElementById("x");
				objDiv.scrollTop = objDiv.scrollHeight;
			</script>

		</div>

	</div> <!-- END class: tab-content -->

</div>

<!-- Infinite scrolling script -->
<script>
	var userLoggedIn = '<?php echo $userLoggedIn; ?>'; //User logged in
	var profileUsername = '<?php echo $profile_username["username"]; ?>'; //User who owns profile

	$(document).ready(function() {

		$('#loading').show(); //Show loading icon

	    //Original ajax request for loading first posts 
		$.ajax({
		    url:"includes/handlers/ajax_load_profile_posts.php",
		    type:"POST",
		    data:"page=1&userLoggedIn=" + userLoggedIn + "&profileUsername=" + profileUsername,
		    cache: false,

		    success: function(data){
				$('#loading').hide(); //Hide loading icon
				$('.swirls_area').html(data); //Insert returned data into div   
			}
		});

		$(window).scroll(function() {

			var height = $('.swirls_area').height(); //Get height of div containing swirls
		    var scroll_top = $(this).scrollTop();
		    var page = $('.swirls_area').find('.nextpage').val();
		    var noMorePosts = $('.swirls_area').find('.noMorePosts').val();

			if ((document.body.scrollHeight == document.body.scrollTop + window.innerHeight) && noMorePosts == 'false') {
		        $('#loading').show(); //Show loading icon

			    var ajaxreq = $.ajax({
				    url:"includes/handlers/ajax_load_profile_posts.php",
			        type:"POST",
			        data:"page=" + page + "&userLoggedIn=" + userLoggedIn + "&profileUsername=" + profileUsername, //Function to call (functionName) and page number
			        cache: false,

			        success: function(response){

						$('.swirls_area').find('.nextpage').remove(); //Remove current .nextPage (hidden input)
					    $('.swirls_area').find('.noMorePosts').remove(); //Remove current .noMorePosts (hidden input)
					    $('#loading').hide(); //Hide loading icon 
					   
					    $('.swirls_area').append(response); //Append with new posts 
					}
				
			    });
		    }

		    return false;
		});
	}); //End document.ready
</script>

<!-- modal window for posting a swirl -->
<div class="modal fade hide" id="post_form" tabindex="-1" role="dialog" aria-labelledby="postModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">

      		<div class="modal-header">
       			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       			<h4 class="modal-title" id="exampleModalLabel">Write a Swirl to <?php echo $profile_user->getFirstAndLastName(); ?></h4>
      		</div> <!-- class: modal header -->

      		<div class="modal-body">
      			<p>This will appear on this user's profile page and also your Swirlfeed for your friends to see. </p>
		        <form class="profile_post" action="" method="POST">
		          	<div class="form-group">
		            	<textarea class="form-control" name="post_body"></textarea>
		            	<input type="hidden" name="user_from" value="<?php echo $userLoggedIn; ?>">
		            	<input type="hidden" name="user_to" value="<?php echo $username; ?>"> <!-- Holds username of user who owns profile -->
		          	</div>
		        </form>
      		</div><!-- class: modal body -->

      		<div class="modal-footer">
	        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        		<button type="button" class="btn btn-primary" name="post_button" id="submit_profile_post">Post Swirl</button>
	        	</form>
      		</div><!-- class: modal footer -->
    	</div><!-- class: modal content -->
  	</div><!-- class: modal dialog -->
</div> <!-- END class: modal fade -->

<?php include("includes/footer.php");?>