<?php 
require 'config/config.php';
require 'includes/functions/email_exists.php'; 
require 'includes/form_handlers/register_handler.php';
require 'includes/form_handlers/login_handler.php'; 

if(isset($_SESSION["username"])){
	header("Location: index.php");
}

?>
<!doctype html>
<html>
<head>
	<title>Swirlfeed</title>
	<link rel="stylesheet" type="text/css" href="assets/css/register_style.css">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="assets/js/register.js"></script>
</head>
<body>

	
<?php 
//Detirmine which form to show the user
if($reg){
	echo '<script>
 	$(document).ready(function(){
	     $("#first").hide();
	     $("#second").show();
	});	
	</script>';
}
?>
	
	<div class="wrapper">
		<div class="login_box">
			<div class="login_header">
				<h1>Swirlfeed!</h1>
				Login or register to get started!
			</div>
			<br>
			<div id="first">
				<form action="register.php" method="POST">
					<label>
						<i class="icon fa fa-user" id="email"></i>
						<input type="email" name="log_email" placeholder="Email" autocorrect="off" autocomplete="off" autocapitalize="off" value="<?php
				if(isset($_SESSION['log_email'])) 
					echo $_SESSION['log_email']; 
				?>" required>
					</label>
					<br>
					<label>
						<i class="icon fa fa-lock" id="password"></i>
						<input type="password" name="log_password" placeholder="Password" required>
					</label>
					<br>
					<?php if(in_array("The email or password you entered is incorrect!<br>", $error_array)) echo "The email or password you entered is incorrect!<br>"; ?>
					<input type="submit" name="login_button" value="Login">
					<br>
					<a href="#" id="signup" class="signup">Need an account? Register here</a>
				</form>
				
			</div>

			<div id="second">
				<form action="register.php" method="POST">
					<label>
						<i class="icon fa fa-user" id="email"></i>
						<input type="text" name="reg_fname" placeholder="First Name" autocorrect="off" autocomplete="off" autocapitalize="off" value="<?php 
				if(isset($_SESSION['reg_fname'])) 
					echo $_SESSION['reg_fname']; 
				?>" required>
					</label>
					<br>
					<?php if(in_array("Your first name must be between 2 and 25 characters!<br>", $error_array)) echo "Your first name must be between 2 and 25 characters!<br>"; ?>
					<label>
						<i class="icon fa fa-user" id="email"></i>
						<input type="text" name="reg_lname" placeholder="Last Name" autocorrect="off" autocomplete="off" autocapitalize="off" value="<?php 
				if(isset($_SESSION['reg_lname'])) 
					echo $_SESSION['reg_lname']; 
				?>" required>
					</label>
					<br>
					<?php if(in_array("Your last name must be between 2 and 25 characters!<br>", $error_array)) echo "Your last name must be between 2 and 25 characters!<br>"; ?>
					<label>
						<i class="icon fa fa-envelope " id="email"></i>
						<input type="email" name="reg_email" placeholder="Email" autocorrect="off" autocomplete="off" autocapitalize="off" value="<?php
				if(isset($_SESSION['reg_email'])) 
					echo $_SESSION['reg_email']; 
				?>" required>
					</label>
					<br>
					<label>
						<i class="icon fa fa-envelope " id="email"></i>
						<input type="email" name="reg_email2" placeholder="Confirm Email" autocorrect="off" autocomplete="off" autocapitalize="off" value="<?php 
				if(isset($_SESSION['reg_email2'])) 
					echo $_SESSION['reg_email2']; 
				?>" required>
					</label>
					<br>
					<?php if(in_array("Invalid email format!<br>", $error_array)) echo "Invalid email format!<br>"; 
					else if(in_array("This email address has already been registered to an account!<br>", $error_array)) echo "This email address has already been registered to an account!<br>";
					else if(in_array("Your E-mails do not match!<br>", $error_array)) echo "Your E-mails do not match!<br>";?>
					<label>
						<i class="icon fa fa-lock" id="password"></i>
						<input type="password" name="reg_password" placeholder="Password" required>
					</label>
					<br>
					<label>
						<i class="icon fa fa-lock" id="password"></i>
						<input type="password" name="reg_password2" placeholder="Confirm Password" required>
					</label>
					<br>
					<?php if(in_array("Your passwords do not match!<br>", $error_array)) echo "Your passwords do not match!<br>";
					else if(in_array("Your password must be between 5 and 30 characters long!<br>", $error_array)) echo "Your password must be between 5 and 30 characters long!<br>";
					else if(in_array("Your password can only contain english letters and numbers!<br>", $error_array)) echo "Your password can only contain english letters and numbers!<br>"; 
					?>
					<input type="submit" name="register_button" value="Register">
					<br>
					<?php if(in_array("<span style='color: #14C800'>You're all set! Go ahead and login!</span><br>", $error_array)) echo "<span style='color: #14C800'>You're all set! Go ahead and login!</span><br>"; ?>
					<a href="#" id="signin" class="signin">Already have an account? Login here</a>
				</form>
			</div>
			<br>
		</div>
	</div>

</body>
</html>