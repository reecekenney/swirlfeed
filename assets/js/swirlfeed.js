function changeHeight(id, height) {
	document.getElementById(id).style.height = height; //Change the height
}

//Gets users as user is typing into the search box. 
//Presents results in a drop down table 
function getLiveSearchUsers(value, user) {

  $.post("includes/handlers/ajax_search.php", {query:value, userLoggedIn: user}, function(data){
    if($(".search_results_footer_empty")[0]){
      $(".search_results_footer_empty").toggleClass("search_results_footer" );
      $(".search_results_footer_empty").toggleClass("search_results_footer_empty" );
    }
    $(".search_results").html(data);
    $(".search_results_footer").html("<a href='search.php?q=" + value + "'>See All Results</a>");
    if(data == ""){
      $(".search_results_footer").html("");
      $(".search_results_footer").toggleClass("search_results_footer_empty" );
      $(".search_results_footer").toggleClass("search_results_footer" );
    }
    
  });
}
//Gets users as user is typing into the search box. 
//Presents results in a drop down table 
function getUsers(value, user) {
  $.post("includes/handlers/ajax_friend_search.php", {query:value, userLoggedIn: user}, function(data){
    $(".results").html(data);
  });
}

//Gets notifications, messages or friend requests for loggedin user. 
//Presents results in a drop down table 
function getDropdownData(user, type) {

  if ($(".dropdown_data_window").css("height") == "0px"){

        var pageName; //Hold name of page to send ajax request to

        if(type == 'notification'){
          pageName = "ajax_load_notifications.php"; //Page to load notifications
          $("span").remove("#unread_notification"); 
        }
        else if(type == 'message'){
          pageName = "ajax_load_messages.php"; //Page to load messages
          $("span").remove("#unread_message"); 
        }
        else if(type == 'friend_request')
          pageName = "ajax_load_friend_requests.php"; //Page to load friend requests 

        var ajaxreq = $.ajax({
              url:"includes/handlers/" + pageName,
                type:"POST",
                data:"page=1&userLoggedIn=" + user, //pass user logged in username
                cache: false,

                success: function(response){

                $('.dropdown_data_window').html(response); //Append with new posts 
                $(".dropdown_data_window").css({"padding": "0px", "height": "280px"});
                $('#dropdown_data_type').val(type); //Set hidden input field to type of data being loaded
            }
        
        });
  }
  else {
    $(".dropdown_data_window").css({"padding": "0px", "height": "0px"});
    $(".dropdown_data_window").html("");
  }
}

$(document).click(function(e){
    //Resize swirl post text box and submit button when user clicks it
    if ($(e.target).is('#swirl_text')) {
            changeHeight("swirl_text", "60px"); //Resize textbox
            changeHeight("swirl_button", "60px");//Resize submit button

    }//Resize swirl post text box and submit button when user clicks off it
    else if($('#swirl_text').length > 0){ //If page contains that div...
        changeHeight("swirl_text", "30px"); //Resize textbox
        changeHeight("swirl_button", "30px") //Resize submit button
    }

    //If user clicks away from search bar and search results, hide drop down results
    if (e.target.class != 'search_results' && e.target.id != 'search-text-input'){
      //Remove drop down results
      $(".search_results").html(""); //Remove drop down results from under search bar
      $(".search_results_footer").html(""); //Remove drop down results footer 
      $(".search_results_footer").toggleClass("search_results_footer_empty" );
      $(".search_results_footer").toggleClass("search_results_footer" );
    }

    //If user clicks away from dropdown data window hide drop down results
    if (e.target.className != 'dropdown_data_window'){
      //Remove drop down window
      $(".dropdown_data_window").css({"padding": "0px", "height": "0px"}); //Remove padding and height
      $(".dropdown_data_window").html(""); //Remove content
    }
});

$(document).ready(function(){  
    //User clicks on search bar
  	$('#search-text-input').focus(function() {
       	$(this).animate({width: '250px'}, 500); //Make width larger
  	});
  
    //Makes the div on the search bar act as a submit button
	  $('.button_holder').on('click', function () {
        document.search_form.submit(); //Submit form 
    });

    //Button to submit profile post
    $("#submit_profile_post").click(function(){
        $.ajax({
            type: "POST",
            url: "includes/handlers/ajax_submit_profile_post.php", 
            data: $('form.profile_post').serialize(),
            success: function(msg){
                $("#post_form").modal('hide'); //hide popup 
                location.reload();
            },
            error: function(){
                alert("failure");
            }
        });
    });
});