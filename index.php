<?php 
include("includes/header.php"); //Header 

if(isset($_POST['swirl'])) {

	//Check if user is on mobile device
	if ($iphone || $android || $palmpre || $ipod || $berry) 
		$is_mobile = "yes";
	else
		$is_mobile = "no";

	//Value of 'hidden mode' checkbox - ticked = yes, not ticked = no
	$isHidden = (isset($_POST['hidden_mode']) == 'yes') ? 'yes' : 'no'; 

	$swirl = new Swirl($con, $userLoggedIn, $is_mobile);
	$swirl->postSwirl($_POST['swirl_text'], $isHidden, 'none', $is_mobile);
}
	//Get weather info 
  	$json_string = file_get_contents("http://api.wunderground.com/api/2b88d665c1a18d58/geolookup/conditions/q/autoip.json");
  	$parsed_json = json_decode($json_string);
  	//Get info from JSON data 
  	$location = $parsed_json->location->city; //City name
  	$temp_f = $parsed_json->current_observation->temp_f; //Temperature in fahrenheight 
  	$temp_c = $parsed_json->current_observation->temp_c; //Temperature in celcius 
  	$feels_temp_f = $parsed_json->current_observation->feelslike_f; //Temperature it feels like in fahrenheight 
  	$feels_temp_c = $parsed_json->current_observation->feelslike_c; //Temperature it feels like in celcius 
  	$wind_mph = $parsed_json->current_observation->wind_gust_mph;
  	$weather = $parsed_json->current_observation->weather;

  	$feels_like_message = ($feels_temp_c != $temp_c) ? "Although it feels like ".$feels_temp_c."&#8451 (".$feels_temp_f."&#8457)<br>" : "<br>";

  echo '<div style="text-align:center;" class="alert alert-success" role="alert">
  			'.$location.' is '.$weather.' and '.$temp_c.'&#8451 ('.$temp_f.'&#8457).
  			'.$feels_like_message.'
  			Wind speed: '.$wind_mph.'mph
  		</div>';
?>

<!-- column for logged in user details on the left -->
<div class="user_details column">
	<a href="<?php echo $user['username']; ?>"><img src="<?php echo $user['profile_pic']; ?>"></a>
	<div class="user_details_left_right">
		<br>
		<a href="<?php echo $user['username']; ?>"><?php echo $user['first_name']." ".$user['last_name']; ?></a>
		<br>
		Posts: <?php echo $user['num_posts']; ?>
		<br>
		Bumps: <?php echo $user['num_bumps']; ?>
		<br>
	</div>

</div>

<div class="main_column column" id="main_column">
	<form class="swirl_form" action="index.php" method="POST">
		<input name="hidden_mode" value="yes" type="checkbox"> Hidden Mode<br>
		<textarea name="swirl_text" id="swirl_text" placeholder="Post a swirl!"></textarea>
		<input type="submit" name="swirl" id="swirl_button" value="Swirl">
		<hr/>
	</form>
	<br>
	<hr style="margin-bottom: 15px;"/>
	<div class="swirls_area"></div>
	<img id='loading' src='assets/images/icons/loading.gif'>
</div>

<!-- Trending words column -->
<div class="user_details column">
	<h4>Popular</h4>
		 <div class="trends">
		 <?
		   $sql=mysqli_query($con, "SELECT * FROM trends ORDER BY hits DESC LIMIT 9");
		   foreach($sql as $r){
			    $query=$r['title'];
			    $wdot = strlen($query)>=14 ? "....":"";
			    $sp_t = str_split($query,14);
			    $sp_t = $sp_t[0];
			    echo '<div style="padding:1px;">';
			    echo $sp_t."".$wdot;
			    echo '<div style="margin-top:5px;"></div>'; // Adds A Break like <br/>
			    echo "</div>";
			}
		?>
		</div>
</div>

<!-- script for loading posts AND infinite scrolling -->
<script>
	var userLoggedIn = '<?php echo $userLoggedIn; ?>';

	$(document).ready(function() {

		$('#loading').show(); //Show loading icon

	    //Original ajax request for loading first posts 
		$.ajax({
		    url:"includes/handlers/ajax_load_posts.php",
		    type:"POST",
		    data:"page=1&userLoggedIn=" + userLoggedIn,
		    cache: false,

		    success: function(data){
				$('#loading').hide(); //Hide loading icon
				$('.swirls_area').html(data); //Insert returned data into div   
			}
		});

		$(window).scroll(function() {

			var height = $('.swirls_area').height(); //Get height of div containing swirls
		    var scroll_top = $(this).scrollTop();
		    var page = $('.swirls_area').find('.nextpage').val();
		    var noMorePosts = $('.swirls_area').find('.noMorePosts').val();

			if ((document.body.scrollHeight == document.body.scrollTop + window.innerHeight) && noMorePosts == 'false') {
		        $('#loading').show(); //Show loading icon

			    var ajaxreq = $.ajax({
				    url:"includes/handlers/ajax_load_posts.php",
			        type:"POST",
			        data:"page=" + page + "&userLoggedIn=" + userLoggedIn, //Function to call (functionName) and page number
			        cache: false,

			        success: function(response){

						$('.swirls_area').find('.nextpage').remove(); //Remove current .nextPage (hidden input)
					    $('.swirls_area').find('.noMorePosts').remove(); //Remove current .noMorePosts (hidden input)
					    $('#loading').hide(); //Hide loading icon 
					   
					    $('.swirls_area').append(response); //Append with new posts 
					}
				
			    });
		    }

		    return false;
		});
	}); //End document.ready
</script>

<?php //include("includes/footer.php");?>