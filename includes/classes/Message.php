<?php
class Message {
	private $con; //MySQL connection variable 
	private $user_obj; //User object for user logged in

	//Constructor
	public function __construct($con, $userLoggedIn) {
		$this->con = $con; //MySQL connection variable 
		$this->user_obj = new User($con, $userLoggedIn); //Username for user logged in
	}

	public function getUnreadNumber(){
		$userLoggedIn = $this->user_obj->getUsername(); //Username of user logged in
		$query = $this->con->query("SELECT id FROM messages WHERE viewed='no' AND user_to='$userLoggedIn'");
		return $query->num_rows;
	}

	//Returns the most recent user that userLoggedIn interacted with
	public function getMostRecentUser(){
		$userLoggedIn = $this->user_obj->getUsername(); //Username for user logged in

		//Get last message interaction involving userLoggedIn
		$get_recent_query = mysqli_query($this->con, "SELECT user_to, user_from FROM messages WHERE user_to='$userLoggedIn' OR user_from='$userLoggedIn' ORDER BY ID DESC LIMIT 1");

		//If the user has not sent or recieved any messages yet
		if(mysqli_num_rows($get_recent_query) == 0)
			return false;
		
		$row = mysqli_fetch_assoc($get_recent_query);

		//Get usernames of interaction
		$user_to = $row['user_to'];
		$user_from = $row['user_from'];

		//Return username which is not the user logged in
		if($user_to != $userLoggedIn)
			return $user_to;
		else 
			return $user_from;
	}

	//Gets messages involving user logged in and user passed as parameter ($other_user)
	public function getMessages($other_user){

		$userLoggedIn = $this->user_obj->getUsername(); //Username for user logged in
		$data = ""; // Holds all data to be returned i.e. all messages concatenated

		//Set opened to yes for all messages for that user. 
	    $set_viewed = $this->con->query("UPDATE messages SET opened='yes' WHERE user_to='$userLoggedIn' 
	    								AND user_from='$other_user'");

		$get_messages_query = mysqli_query($this->con, "SELECT * FROM messages WHERE (user_to='$userLoggedIn' AND user_from='$other_user') OR (user_from='$userLoggedIn' AND user_to='$other_user') ORDER BY id ASC");
		while($row = mysqli_fetch_assoc($get_messages_query)){
			$user_to = $row['user_to'];
			$user_from = $row['user_from'];
			$body = $row['body'];

			//if user_to is the user logged in, make div green and put on the left, else make div blue
			$div_top = ($user_to == $userLoggedIn) ? "<div class='message' id='green'>" : "<div class='message' id='blue'>";
			$data = $data . $div_top . $body . "</div><br><br>"; //Concatenate with message body
		}

		return $data;
	}

	//Sends message 
	public function sendMessage($user_to, $body, $date){
		if($body != ""){
			$userLoggedIn = $this->user_obj->getUsername(); //Username for user logged in
			$send_message_query = mysqli_query($this->con, "INSERT INTO messages VALUES ('', '$user_to', '$userLoggedIn', '$body', '$date', 'no', 'no', 'no')");
		}
	}
	//Gets recent conversations in order. *** Not for drop down *** NO INFINITE SCROLL
	public function getConvos() {
		$userLoggedIn = $this->user_obj->getUsername(); //Username for user logged in
		$return_string = ""; //String to hold data that will be returned
		$convos = array(); //Array for usernames of conversations 

		$get_convos_query = mysqli_query($this->con, "SELECT user_to, user_from FROM messages WHERE user_to='$userLoggedIn' OR user_from='$userLoggedIn' ORDER BY id DESC");

		while($row = mysqli_fetch_assoc($get_convos_query)){
			//Check if user logged in sent or received last message 
			$user_to_push = ($row['user_to'] != $userLoggedIn) ? $row['user_to'] : $row['user_from'];

			//If username is not already in array, put it in
			if(!in_array($user_to_push, $convos))
				array_push($convos, $user_to_push);
		}

		//Array of usernames that userloggedin has conversed with
		foreach ($convos as $username) {

			$user_found_obj = new User($this->con, $username); //User object for user found
			$latest_message_details = $this->getLatestMessage($userLoggedIn, $username); //get latest message between userlogged in and user found
			//latest_message_details[0] = who sent message i.e. 'You said: ' or 'They said: '
			//latest_message_details[1] = body of message
			//latest_message_details[2] = time message was sent relative to the current time

			$dots = strlen($latest_message_details[1]) >= 12 ? "....":""; //If message body is greater than 11, add '...'
		    $split = str_split($latest_message_details[1], 12); //Split message at 13 characters 
		    $split = $split[0].$dots;
			$return_string .= "<a href='messages.php?u=$username'><div class='user_found_messages'>
								<img src='".$user_found_obj->getProfilePic()."' style='border-radius: 5px; margin-right: 5px;'>
								".$user_found_obj->getFirstAndLastName()." 
								<span class='timestamp_smaller' id='grey' >".$latest_message_details[2]."</span>
								<p id='grey' style='margin: 0;'>".$latest_message_details[0].$split." </p>
								</div>
								</a>";
		}

		return $return_string;
	}

	//Gets recent conversations in order *** FOR drop down *** INCLUDES INFINITE SCROLL
	public function getConvosDropdown($data, $limit) {
		$page = $data['page']; //Page number passed as parameter
		$userLoggedIn = $this->user_obj->getUsername(); //Username for user logged in
		$return_string = ""; //String to hold data that will be returned
		$convos = array(); //Array for usernames of conversations 

		if($page == 1){
	        $start = 0;  //Start at first post
	    }
	    else{
	        $start = ($page - 1) * $limit; //Start where last loaded posts left off
	    }

	    //Set viewed to yes for all messages for that user. 
	    //Viewed mysql column is when user opens notification window
	    //Opened mysql column is when user actually clicks on notification 
	    $set_viewed = $this->con->query("UPDATE messages SET viewed='yes' WHERE user_to='$userLoggedIn'");

		$get_convos_query = mysqli_query($this->con, "SELECT user_to, user_from FROM messages WHERE user_to='$userLoggedIn' OR user_from='$userLoggedIn' ORDER BY id DESC");

		while($row = mysqli_fetch_assoc($get_convos_query)){
			//Check if user logged in sent or received last message
			$user_to_push = ($row['user_to'] != $userLoggedIn) ? $row['user_to'] : $row['user_from'];
			

			//If username is not already in array, put it in
			if(!in_array($user_to_push, $convos))
				array_push($convos, $user_to_push);
		}

		$num_iterations = 0; //Number of messages checked (not neccassarily posted)
	    $count = 1; //Number of messages posted
		//Array of usernames that userloggedin has conversed with
		foreach ($convos as $username) {
			if($num_iterations++ < $start)
        		continue;

        	//Once 5 notifications have been loaded, stop
        	if($count > $limit)
        		break;
        	else
        		$count++; //Number of notifications loaded + 1

        	$is_unread_query = mysqli_query($this->con, "SELECT opened FROM messages WHERE user_to='$userLoggedIn' AND user_from='$username' ORDER BY id DESC");
	    	$row = $is_unread_query->fetch_array(MYSQLI_ASSOC);
	    	$style = ($row['opened'] == 'no') ? "background-color: #DDEDFF;" : ""; //If unopened, change background color slightly

			$user_found_obj = new User($this->con, $username); //User object for user found
			$latest_message_details = $this->getLatestMessage($userLoggedIn, $username); //get latest message between userlogged in and user found
			//latest_message_details[0] = who sent message i.e. 'You said: ' or 'They said: '
			//latest_message_details[1] = body of message
			//latest_message_details[2] = time message was sent relative to the current time

			$dots = strlen($latest_message_details[1]) >= 12 ? "....":""; //If message body is greater than 11, add '...'
		    $split = str_split($latest_message_details[1], 12); //Split message at 13 characters 
		    $split = $split[0].$dots;
			$return_string .= "<a href='messages.php?u=$username'>
									<div class='user_found_messages' style='".$style."'>
									<img src='".$user_found_obj->getProfilePic()."' style='border-radius: 5px; margin-right: 5px;'>
									".$user_found_obj->getFirstAndLastName()." 
									<span class='timestamp_smaller' id='grey' >".$latest_message_details[2]."</span>
									<p id='grey' style='margin: 0;'>".$latest_message_details[0].$split." </p>
									</div>
								</a>";
		}

		//If posts were loaded
	        if($count > $limit)
	        	//Holds value of next page. Must stay hidden
	        	$return_string.="<input type='hidden' class='nextpageDropdownData' value='".($page + 1)."'><input type='hidden' class='noMoreDropdownData' value='false'>";
	        else 
	        	//No more Notifications to load. Show 'Finished' message
	        	$return_string .= "<input type='hidden' class='noMoreDropdownData' value='true'><p style='text-align: center;'>No more messages to load!</p>";

		return $return_string;
	}

	//Returns the latest message between two given users 
	public function getLatestMessage($userLoggedIn, $user2) {
		$details_array = array();
		//Gets latest message 
		$query = $this->con->query("SELECT body, user_to, date FROM messages WHERE (user_to='$userLoggedIn' AND user_from='$user2') OR 
									(user_to='$user2' AND user_from='$userLoggedIn') ORDER BY id DESC LIMIT 1");
		$row = $query->fetch_array(MYSQLI_ASSOC);
		$sent_by = ($row['user_to'] == $userLoggedIn) ? "They said: " : "You said: "; //Message that shows if userLoggedIn sent or received message

		$date_time_now = date("Y-m-d H:i:s");
        $start_date = new DateTime($row['date']);
        $end_date = new DateTime($date_time_now);
        $interval = $start_date->diff($end_date);
        if($interval->y >= 1){
                if($interval->y == 1)
                        $time_message = $interval->y." year ago";
                else                
                        $time_message = $interval->y." years ago";
        }                
        else if($interval->m >= 1){
                if($interval->d == 0)
                        $days = " ago";
                else if($interval->d == 1)
                        $days = $interval->d." day ago";
                else
                        $days = $interval->d." days ago";
                if($interval->m == 1)
                        $time_message = $interval->m." month ".$days;
                else
                        $time_message = $interval->m." months ".$days;
        }                
        else if($interval->d >= 1){
                if($interval->d == 1)
                        $time_message = "Yesterday";
                else
                        $time_message = $interval->d." days ago";
        }                
        else if($interval->h >= 1){
                if($interval->h == 1)
                        $time_message = $interval->h." hour ago";
                else
                        $time_message = $interval->h." hours ago";
        } 
        else if($interval->i >= 1){
                if($interval->i == 1)
                        $time_message = $interval->i." minute ago";
                else
                        $time_message = $interval->i." minutes ago";
        }   
        else {
                if($interval->s < 30)
                        $time_message = "Just now";
                else
                        $time_message = $interval->s." seconds ago";
        }  

		array_push($details_array, $sent_by);
		array_push($details_array, $row['body']);
		array_push($details_array, $time_message);


		return $details_array; //Return array of information
	}

}