<?php 
class Swirl {
	private $con; //MySQL connection variable 
	private $user_obj; //User object for user logged in

	//Constructor
	public function __construct($con, $userLoggedIn) {
		$this->con = $con; //MySQL connection variable 
		$this->user_obj = new User($con, $userLoggedIn); //Username for user logged in
	}

	//Gets a single swirl
	public function getSingleSwirl($swirl_id){
		$userLoggedIn = $this->user_obj->getUsername(); //Username for user logged in user

        //Set viewed to yes for all notifications for that user. 
        //Viewed mysql column is when user opens notification window
        //Opened mysql column is when user actually clicks on notification 
        $set_viewed = $this->con->query("UPDATE notifications SET opened='yes' WHERE user_to='$userLoggedIn' 
                                        AND link LIKE '%=$swirl_id'");

		$str = ''; //Initialise string that holds data to return
	    $data = $this->con->query("SELECT * FROM swirls WHERE id='$swirl_id' AND deleted='no'");

	    //If query returns empty, no more posts to load
	    if($data->num_rows != 0){

	        $row = $data->fetch_array(MYSQLI_ASSOC);

            $id = $swirl_id;
            $body = $row['body'];
            $added_by = $row['added_by'];
            $date_time = $row['date_added'];
            $mobile_device = $row['mobile_device'];
            $hidden_mode = $row['hidden_mode'];
            //Prepare the user_to string so it can be echo even if post is not to a user
			if($row['user_to'] == 'none')
				$user_to_string = ""; 
			else {
				$user_to_obj = new User($this->con, $row['user_to']); //User object for user swirl is to
				$user_to_name = $user_to_obj->getFirstAndLastName(); //Get first and last name
				$user_to_string = " to <a href='".$row['user_to']."'>".$user_to_name."</a>";
			}

            //Check if user who posted has their account closed. If so, do not show post.
            $added_by_user = new User($this->con, $added_by); //User object for user who posted swirl
            if($added_by_user->isClosed() == "yes"){
            	echo "We're sorry but this user has closed their account.";
            	return;
            }
            
            //Check if user us friends with person who posted
            if($this->user_obj->isFriend($added_by)) {

                //If user posted the swirl, show delete button
                if($userLoggedIn == $added_by)
                    $delete_button = "<button class='delete_button btn-danger' id='post$id'>X</button>";
                else 
                    $delete_button = "";

                $get_user_details = mysqli_query($this->con, "SELECT first_name, last_name, profile_pic FROM users WHERE username='$added_by'");
                $userRow = mysqli_fetch_assoc($get_user_details);
                //If hidden mode = no, use profile pic of user, else use random picture
                if($hidden_mode == 'no'){
                    $firstName = $userRow['first_name'];
                    $lastName = $userRow['last_name'];
                    $profile_pic = $userRow['profile_pic'];
                }
                else {
                    $firstName = 'Someone said: ';
                    $lastName = '';
                    $added_by = '';//Needed so 'Someone said: ' does not link to users profile
                    $random_num = rand(1, 16); //Random numberbetween 1 and 3

                    switch ($random_num) {
                        case '1':
                            $profile_pic = "assets/images/profile_pics/defaults/head_alizarin.png";
                            break;
                        case '2':
                            $profile_pic = "assets/images/profile_pics/defaults/head_amethyst.png";
                            break;
                        case '3':
                            $profile_pic = "assets/images/profile_pics/defaults/head_belize_hole.png";
                            break;
                        case '4':
                            $profile_pic = "assets/images/profile_pics/defaults/head_carrot.png";
                            break;
                        case '5':
                            $profile_pic = "assets/images/profile_pics/defaults/head_deep_blue.png";
                            break;
                        case '6':
                            $profile_pic = "assets/images/profile_pics/defaults/head_emerald.png";
                            break;
                        case '7':
                            $profile_pic = "assets/images/profile_pics/defaults/head_green_sea.png";
                            break;
                        case '8':
                            $profile_pic = "assets/images/profile_pics/defaults/head_nephritis.png";
                            break;
                        case '9':
                            $profile_pic = "assets/images/profile_pics/defaults/head_pete_river.png";
                            break;
                        case '10':
                            $profile_pic = "assets/images/profile_pics/defaults/head_pomegranate.png";
                            break;
                        case '11':
                            $profile_pic = "assets/images/profile_pics/defaults/head_pumpkin.png";
                            break;
                        case '12':
                            $profile_pic = "assets/images/profile_pics/defaults/head_red.png";
                            break;
                        case '13':
                            $profile_pic = "assets/images/profile_pics/defaults/head_sun_flower.png";
                            break;
                        case '14':
                            $profile_pic = "assets/images/profile_pics/defaults/head_turqoise.png";
                            break;
                        case '15':
                            $profile_pic = "assets/images/profile_pics/defaults/head_wet_asphalt.png";
                            break;
                        case '16':
                            $profile_pic = "assets/images/profile_pics/defaults/head_wisteria.png";
                            break;
                    } //End switch
                } //End is hidden mode
                ?>

                <script language="javascript">
                    function toggle<?php echo $id; ?>() {
                        var target = $( event.target );
                        if ( !target.is( "a" ) ) {
                            var ele = document.getElementById("toggleComment<?php echo $id; ?>");
                            if(ele.style.display == "block") {
                                ele.style.display = "none";
                            }
                            else {
                                ele.style.display = "block";
                            }
                        }
                    }
                </script>
                <?php

                $comments_check = mysqli_query($this->con, "SELECT * FROM swirl_comments WHERE post_id='$id'");
                $comments_check_num_rows = mysqli_num_rows($comments_check);

                $date_time_now = date("Y-m-d H:i:s");
                $start_date = new DateTime($date_time);
                $end_date = new DateTime($date_time_now);
                $interval = $start_date->diff($end_date);
                if($interval->y >= 1){
                        if($interval->y == 1)
                                $time_message = $interval->y." year ago";
                        else                
                                $time_message = $interval->y." years ago";
                }                
                else if($interval->m >= 1){
                        if($interval->d == 0)
                                $days = " ago";
                        else if($interval->d == 1)
                                $days = $interval->d." day ago";
                        else
                                $days = $interval->d." days ago";
                        if($interval->m == 1)
                                $time_message = $interval->m." month ".$days;
                        else
                                $time_message = $interval->m." months ".$days;
                }                
                else if($interval->d >= 1){
                        if($interval->d == 1)
                                $time_message = "Yesterday";
                        else
                                $time_message = $interval->d." days ago";
                }                
                else if($interval->h >= 1){
                        if($interval->h == 1)
                                $time_message = $interval->h." hour ago";
                        else
                                $time_message = $interval->h." hours ago";
                } 
                else if($interval->i >= 1){
                        if($interval->i == 1)
                                $time_message = $interval->i." minute ago";
                        else
                                $time_message = $interval->i." minutes ago";
                }   
                else {
                        if($interval->s < 30)
                                $time_message = "Just now";
                        else
                                $time_message = $interval->s." seconds ago";
                }  

                //Check if post came from mobile device
                if($mobile_device == "yes")
                    $from_mobile = "&nbsp;&nbsp;&nbsp;Via mobile device";
                else 
                    $from_mobile = "";

                $str.= "<br>
                        <div class='swirl_post' onClick='javascript:toggle$id()'>
                            <div class='swirl_profile_pic'>
                                <img src='$profile_pic' width='50'>
                            </div>
                            <div class='posted_by' style='color: #ACACAC;'>
                                <a href='$added_by'> $firstName $lastName </a> ".$user_to_string." &nbsp;&nbsp;&nbsp;$time_message $from_mobile
                                $delete_button
                            </div>
                            <div id='post_body'>$body<br/><br/></div>

                            <div class='newsfeedPostOptions'>
                                Comments ($comments_check_num_rows)&nbsp;&nbsp;&nbsp
                                <iframe src='swirl_bump.php?post_id=$id' scrolling='no'> </iframe>
                            </div>
            
                        </div>
                        <div class='swirl_comment' id='toggleComment$id' style='display: none;'>
                            <iframe src='./swirl_comment_frame.php?post_id=$id' frameborder='0' id='comment_iframe'></iframe>
                        </div>
                        <br>
                        <hr style='margin:0';/>
                        <br>
                        ";

                        ?>
                        <script>
                        $(document).ready(function(){
                            $('#post<?php echo $id; ?>').on('click', function(){
                                bootbox.confirm("Are you sure you want to delete this swirl?", function(result) {
                                    $.post("includes/form_handlers/delete_swirl.php?post_id=<?php echo $id; ?>",{result:result});

                                    if(result)
                                        location.reload();
                                }); 
                            });
                        });
                        </script>
                        <?php             
            } //Check if user is friends with person who posted*/
            else {
            	echo "You cannot see this post as you are not friends with this user.";
            	return;
            }

	    } //if num_rows != 0
	    else { 
	        //No more posts to load. Show 'Finished' message
	        echo "<p>No post was found. If you clicked on a link it may be broken.</p>";
	        return;
	    }
	  
	    //Show swirls
	    echo $str; 
	}//End get single post 

	//Gets swirls by friends
	public function loadPostsFriends($data, $limit) {

	    $page = $data['page']; //Page number passed as parameter
	    $userLoggedIn = $this->user_obj->getUsername();

	    if($page == 1){
	        $start = 0;  //Start at first post
	    }
	    else{
	        $start = ($page - 1) * $limit; //Start where last loaded posts left off
	    }
	    
	    $str = ''; //Initialise string that holds data to return
	    $data = $this->con->query("SELECT * FROM swirls WHERE deleted='no' ORDER BY id DESC");

	    //If query returns empty, no more posts to load
	    if($data->num_rows > 0){

	    	$num_iterations = 0; //Number of results checked (not neccassarily posted)
	    	$count = 1;
	        while( $row = $data->fetch_array(MYSQLI_ASSOC) ){

	            $id = $row['id'];
	            $body = $row['body'];
	            $added_by = $row['added_by'];
	            $date_time = $row['date_added'];
	            $mobile_device = $row['mobile_device'];
	            $hidden_mode = $row['hidden_mode'];
	            //Prepare the user_to string so it can be echo even if post is not to a user
				if($row['user_to'] == 'none')
					$user_to_string = ""; 
				else {
					$user_to_obj = new User($this->con, $row['user_to']); //User object for user swirl is to
					$user_to_name = $user_to_obj->getFirstAndLastName(); //Get first and last name
					$user_to_string = " to <a href='".$row['user_to']."'>".$user_to_name."</a>";
				}

	            //Check if user who posted has their account closed. If so, do not show post.
	            $added_by_user = new User($this->con, $added_by); //User object for user who posted swirl
	            if($added_by_user->isClosed() == "yes")
	                continue;

	            //User object with username of user logged in 
	            $logged_in_user = new User($this->con, $userLoggedIn);
	            
	            //Check if user us friends with person who posted
	            if($logged_in_user->isFriend($added_by)) {

	            	//If not reached start position from last loads yet
		        	if($num_iterations++ < $start)
		        		continue;

	            	//Once 10 swirls have been loaded
	            	if($count > $limit)
	            		break;
	            	else 
	            		$count++;
	            		

	                //If user posted the swirl, show delete button
	                if($userLoggedIn == $added_by)
	                    $delete_button = "<button class='delete_button btn-danger' id='post$id'>X</button>";
	                else 
	                    $delete_button = "";

	                $get_user_details = mysqli_query($this->con, "SELECT first_name, last_name, profile_pic FROM users WHERE username='$added_by'");
	                $userRow = mysqli_fetch_assoc($get_user_details);
	                //If hidden mode = no, use profile pic of user, else use random picture
	                if($hidden_mode == 'no'){
	                    $firstName = $userRow['first_name'];
	                    $lastName = $userRow['last_name'];
	                    $profile_pic = $userRow['profile_pic'];
	                }
	                else {
	                    $firstName = 'Someone said: ';
	                    $lastName = '';
	                    $added_by = '';//Needed so 'Someone said: ' does not link to users profile
	                    $random_num = rand(1, 16); //Random numberbetween 1 and 3

	                    switch ($random_num) {
	                        case '1':
	                            $profile_pic = "assets/images/profile_pics/defaults/head_alizarin.png";
	                            break;
	                        case '2':
	                            $profile_pic = "assets/images/profile_pics/defaults/head_amethyst.png";
	                            break;
	                        case '3':
	                            $profile_pic = "assets/images/profile_pics/defaults/head_belize_hole.png";
	                            break;
	                        case '4':
	                            $profile_pic = "assets/images/profile_pics/defaults/head_carrot.png";
	                            break;
	                        case '5':
	                            $profile_pic = "assets/images/profile_pics/defaults/head_deep_blue.png";
	                            break;
	                        case '6':
	                            $profile_pic = "assets/images/profile_pics/defaults/head_emerald.png";
	                            break;
	                        case '7':
	                            $profile_pic = "assets/images/profile_pics/defaults/head_green_sea.png";
	                            break;
	                        case '8':
	                            $profile_pic = "assets/images/profile_pics/defaults/head_nephritis.png";
	                            break;
	                        case '9':
	                            $profile_pic = "assets/images/profile_pics/defaults/head_pete_river.png";
	                            break;
	                        case '10':
	                            $profile_pic = "assets/images/profile_pics/defaults/head_pomegranate.png";
	                            break;
	                        case '11':
	                            $profile_pic = "assets/images/profile_pics/defaults/head_pumpkin.png";
	                            break;
	                        case '12':
	                            $profile_pic = "assets/images/profile_pics/defaults/head_red.png";
	                            break;
	                        case '13':
	                            $profile_pic = "assets/images/profile_pics/defaults/head_sun_flower.png";
	                            break;
	                        case '14':
	                            $profile_pic = "assets/images/profile_pics/defaults/head_turqoise.png";
	                            break;
	                        case '15':
	                            $profile_pic = "assets/images/profile_pics/defaults/head_wet_asphalt.png";
	                            break;
	                        case '16':
	                            $profile_pic = "assets/images/profile_pics/defaults/head_wisteria.png";
	                            break;
	                    } //End switch
	                } //End is hidden mode
	                ?>

	                <script language="javascript">
	                    function toggle<?php echo $id; ?>() {
	                        var target = $( event.target );
	                        if ( !target.is( "a" ) ) {
	                            var ele = document.getElementById("toggleComment<?php echo $id; ?>");
	                            if(ele.style.display == "block") {
	                                ele.style.display = "none";
	                            }
	                            else {
	                                ele.style.display = "block";
	                            }
	                        }
	                    }
	                </script>
	                <?php

	                $comments_check = mysqli_query($this->con, "SELECT * FROM swirl_comments WHERE post_id='$id'");
	                $comments_check_num_rows = mysqli_num_rows($comments_check);

	                $date_time_now = date("Y-m-d H:i:s");
	                $start_date = new DateTime($date_time);
	                $end_date = new DateTime($date_time_now);
	                $interval = $start_date->diff($end_date);
	                if($interval->y >= 1){
	                        if($interval->y == 1)
	                                $time_message = $interval->y." year ago";
	                        else                
	                                $time_message = $interval->y." years ago";
	                }                
	                else if($interval->m >= 1){
	                        if($interval->d == 0)
	                                $days = " ago";
	                        else if($interval->d == 1)
	                                $days = $interval->d." day ago";
	                        else
	                                $days = $interval->d." days ago";
	                        if($interval->m == 1)
	                                $time_message = $interval->m." month ".$days;
	                        else
	                                $time_message = $interval->m." months ".$days;
	                }                
	                else if($interval->d >= 1){
	                        if($interval->d == 1)
	                                $time_message = "Yesterday";
	                        else
	                                $time_message = $interval->d." days ago";
	                }                
	                else if($interval->h >= 1){
	                        if($interval->h == 1)
	                                $time_message = $interval->h." hour ago";
	                        else
	                                $time_message = $interval->h." hours ago";
	                } 
	                else if($interval->i >= 1){
	                        if($interval->i == 1)
	                                $time_message = $interval->i." minute ago";
	                        else
	                                $time_message = $interval->i." minutes ago";
	                }   
	                else {
	                        if($interval->s < 30)
	                                $time_message = "Just now";
	                        else
	                                $time_message = $interval->s." seconds ago";
	                }  

	                //Check if post came from mobile device
	                if($mobile_device == "yes")
	                    $from_mobile = "&nbsp;&nbsp;&nbsp;Via mobile device";
	                else 
	                    $from_mobile = "";

	                //String containing post
	                $str.= "
	                        <div class='swirl_post' onClick='javascript:toggle$id()'>
	                            <div class='swirl_profile_pic'>
	                                <img src='$profile_pic' width='50'>
	                            </div>
	                            <div class='posted_by' style='color: #ACACAC;'>
	                                <a href='$added_by'> $firstName $lastName </a> ".$user_to_string." &nbsp;&nbsp;&nbsp;$time_message $from_mobile
	                                $delete_button
	                            </div>
	                            <div id='post_body'>$body<br/><br/></div>

	                            <div class='newsfeedPostOptions'>
	                                Comments ($comments_check_num_rows)&nbsp;&nbsp;&nbsp
	                                <iframe src='swirl_bump.php?post_id=$id' scrolling='no'> </iframe>
	                            </div>
	            
	                        </div>
	                        <div class='swirl_comment' id='toggleComment$id' style='display: none;'>
	                            <iframe src='./swirl_comment_frame.php?post_id=$id' frameborder='0' id='comment_iframe'></iframe>
	                        </div>
	                        <br>
	                        <hr style='margin:0';/>
	                        <br>
	                        ";

	                        ?>
	                        <script>
	                        $(document).ready(function(){
	                            $('#post<?php echo $id; ?>').on('click', function(){
	                                bootbox.confirm("Are you sure you want to delete this swirl?", function(result) {
	                                    $.post("includes/form_handlers/delete_swirl.php?post_id=<?php echo $id; ?>",{result:result});

	                                    if(result)
	                                        location.reload();
	                                }); 
	                            });
	                        });
	                        </script>
	                        <?php             

	            } //Check if user is friends with person who posted*/
	        }// End while loop 

	        //If posts were loaded
	        if($count > $limit)
	        	//Holds value of next page. Must stay hidden
	        	$str.="<input type='hidden' class='nextpage' value='".($page + 1)."'><input type='hidden' class='noMorePosts' value='false'>";
	        else 
	        	//No more posts to load. Show 'Finished' message
	        	$str .= "<input type='hidden' class='noMorePosts' value='true'><p style='text-align: center;'>No More posts to show!</p>";
	    }
	  
	    //Show swirls
	    echo $str; 
	} //End load posts friend function

	//Post swirl
	public function postSwirl($body, $isHidden, $user_to, $is_mobile){
		$body = strip_tags($body); //Remove html tags
		$body = $this->con->real_escape_string($body); //Escape all special characters
		$check_empty = preg_replace('/\s+/', '',$body); //Remove all spaces, tabs etc.

		//If text body is not empty
		if($check_empty != ""){

			//Check if user posted youtube link
			$body_array = preg_split("/\s+/", $body); //Split text body into array at spaces " "

			//Iterate through array
			foreach($body_array as $key => $value){

				//Find the element with the youtube link
				if(strpos($value, "www.youtube.com/watch?v=") !== false){
					//Youtube link found
					$value = preg_replace("!watch\?v=!", "embed/", $value); //Change 'watch' to 'embed'
					$value = "<br><iframe width=\'420\' height=\'315\' src=\'" . $value . "\'></iframe><br>"; //Enclose in iframe
					$body_array[$key] = $value; //Set array element to new value
				}
			}

			$body = implode(" ", $body_array);

			//Current date and time
			$date_added = date("Y-m-d H:i:s");
			//Name of user who posted swirl
			$added_by = $this->user_obj->getUsername();

			//If user is on own profile. The post is not to a particular user
			if($user_to == $added_by)
				$user_to = 'none';

			//Insert the post
			$query = $this->con->query("INSERT INTO swirls VALUES('', '$body', '$added_by', '$user_to', '$date_added', '$isHidden', 'no', 'no', '0', '$is_mobile')");
			$returned_id = $this->con->insert_id; //Id of post that was just inserted

			//If user posted to another user, inert notification
			if($user_to != 'none') {
				$notification = new Notification($this->con, $added_by); //Notification object
				$notification->insertNotification($returned_id, $user_to, "profile_post"); //Insert notification
			}

			//update posts count for user
			$user_data_query = $this->con->query("SELECT num_posts FROM users WHERE username='$added_by'");
			$user_data = $user_data_query->fetch_array(MYSQLI_ASSOC);
			$num_of_posts = $user_data['num_posts'];
			$num_of_posts++;
			$update_posts_query = $this->con->query("UPDATE users SET num_posts='$num_of_posts' WHERE username='$added_by'");

			//Update trends
			//Words to remove from post
			$stopWords = "a about above across after again against all almost alone along already
			 also although always among an and another any anybody anyone anything anywhere are 
			 area areas around as ask asked asking asks at away b back backed backing backs be became
			 because become becomes been before began behind being beings best better between big 
			 both but by c came can cannot case cases certain certainly clear clearly come could
			 d did differ different differently do does done down down downed downing downs during
			 e each early either end ended ending ends enough even evenly ever every everybody
			 everyone everything everywhere f face faces fact facts far felt few find finds first
			 for four from full fully further furthered furthering furthers g gave general generally
			 get gets give given gives go going good goods got great greater greatest group grouped
			 grouping groups h had has have having he her here herself high high high higher
		     highest him himself his how however i im if important in interest interested interesting
			 interests into is it its itself j just k keep keeps kind knew know known knows
			 large largely last later latest least less let lets like likely long longer
			 longest m made make making man many may me member members men might more most
			 mostly mr mrs much must my myself n necessary need needed needing needs never
			 new new newer newest next no nobody non noone not nothing now nowhere number
			 numbers o of off often old older oldest on once one only open opened opening
			 opens or order ordered ordering orders other others our out over p part parted
			 parting parts per perhaps place places point pointed pointing points possible
			 present presented presenting presents problem problems put puts q quite r
			 rather really right right room rooms s said same saw say says second seconds
			 see seem seemed seeming seems sees several shall she should show showed
			 showing shows side sides since small smaller smallest so some somebody
			 someone something somewhere state states still still such sure t take
			 taken than that the their them then there therefore these they thing
			 things think thinks this those though thought thoughts three through
	         thus to today together too took toward turn turned turning turns two
			 u under until up upon us use used uses v very w want wanted wanting
			 wants was way ways we well wells went were what when where whether
			 which while who whole whose why will with within without work
			 worked working works would x y year years yet you young younger
			 youngest your yours z lol haha omg fuck fucking shit crap black hey 
			 ill iframe wonder else like hate sleepy reason for some little no yes bye
			 choose";
			//Convert stop words into array - split at white space
			$stopWords = preg_split("/[\s,]+/", $stopWords);

			//Remove all punctionation
			$no_punctuation = preg_replace("/[^a-zA-Z 0-9]+/", "", $body);

			//Predict whether user is posting a url. If so, do not check for trending words
			if(strpos($no_punctuation, "height") === false && strpos($no_punctuation, "width") === false
				&& strpos($no_punctuation, "http") === false && strpos($no_punctuation, "youtube") === false){
				//Convert users post (with punctuation removed) into array - split at white space
				$keywords = preg_split("/[\s,]+/", $no_punctuation);

				foreach($stopWords as $value) {
					foreach($keywords as $key => $value2){
						if(strtolower($value) == strtolower($value2))
							$keywords[$key] = "";
					}
				}

				foreach ($keywords as $value) {
				    $this->calculateTrend(ucfirst($value));
				}
			}
		} //End if checkempty
	}

	public function calculateTrend($term) {
		/* Check if word is already in the tends table */
		if($term != ''){
			$query = $this->con->query("SELECT * FROM trends WHERE title='$term'");
			/* If There isn't a search query in table, insert it */
			if($query->num_rows == 0)
				$query = $this->con->query("INSERT INTO trends(title,hits) VALUES('$term','1')");
			else 
				$query = $this->con->query("UPDATE trends SET hits=hits+1 WHERE title='$term'");
		}
	}

	//Gets swirls by friends
	public function loadPostsProfilePage($data, $limit) {

	    $page = $data['page']; //Page number passed as parameter
	    $profileUser = $data['profileUsername']; //Username of user who owns profile
	    $userLoggedIn = $this->user_obj->getUsername(); //Username of user logged in 

	    if($page == 1){
	        $start = 0;  //Start at first post
	    }
	    else{
	        $start = ($page - 1) * $limit; //Start where last loaded posts left off
	    }
	    
	    $str = ''; //Initialise string that holds data to return
	    $data = $this->con->query("SELECT * FROM swirls WHERE ((added_by='$profileUser' AND user_to='none') OR user_to='$profileUser')
	    						AND hidden_mode='no' AND deleted='no' ORDER BY id DESC");

	    //If query returns empty, no more posts to load
	    if($data->num_rows > 0){

	    	$num_iterations = 0; //Number of results checked (not neccassarily posted)
	    	$count = 1; //Number of results posted
	        while( $row = $data->fetch_array(MYSQLI_ASSOC) ){

	            $id = $row['id'];
	            $body = $row['body'];
	            $added_by = $row['added_by'];
	            $date_time = $row['date_added'];
	            $mobile_device = $row['mobile_device'];

            	//If not reached start position from last loads yet
	        	if($num_iterations++ < $start)
	        		continue;

            	//Once 10 swirls have been loaded
            	if($count > $limit)
            		break;
            	else 
            		$count++; //Number of swirls loaded + 1

                //If user posted the swirl, show delete button
                if($userLoggedIn == $added_by)
                    $delete_button = "<button class='delete_button btn-danger' id='post$id'>X</button>";
                else 
                    $delete_button = "";

                $get_user_details = $this->con->query("SELECT first_name, last_name, profile_pic FROM users WHERE username='$added_by'");
                $userRow = $get_user_details->fetch_array(MYSQLI_ASSOC);

                $firstName = $userRow['first_name'];
                $lastName = $userRow['last_name'];
                $profile_pic = $userRow['profile_pic'];
                ?>

                <script language="javascript">
                    function toggle<?php echo $id; ?>() {
                        var target = $( event.target );
                        if ( !target.is( "a" ) ) {
                            var ele = document.getElementById("toggleComment<?php echo $id; ?>");
                            if(ele.style.display == "block") {
                                ele.style.display = "none";
                            }
                            else {
                                ele.style.display = "block";
                            }
                        }
                    }
                </script>
                <?php

                $comments_check = mysqli_query($this->con, "SELECT * FROM swirl_comments WHERE post_id='$id'");
                $comments_check_num_rows = mysqli_num_rows($comments_check);

                $date_time_now = date("Y-m-d H:i:s");
                $start_date = new DateTime($date_time);
                $end_date = new DateTime($date_time_now);
                $interval = $start_date->diff($end_date);
                if($interval->y >= 1){
                        if($interval->y == 1)
                                $time_message = $interval->y." year ago";
                        else                
                                $time_message = $interval->y." years ago";
                }                
                else if($interval->m >= 1){
                        if($interval->d == 0)
                                $days = " ago";
                        else if($interval->d == 1)
                                $days = $interval->d." day ago";
                        else
                                $days = $interval->d." days ago";
                        if($interval->m == 1)
                                $time_message = $interval->m." month ".$days;
                        else
                                $time_message = $interval->m." months ".$days;
                }                
                else if($interval->d >= 1){
                        if($interval->d == 1)
                                $time_message = "Yesterday";
                        else
                                $time_message = $interval->d." days ago";
                }                
                else if($interval->h >= 1){
                        if($interval->h == 1)
                                $time_message = $interval->h." hour ago";
                        else
                                $time_message = $interval->h." hours ago";
                } 
                else if($interval->i >= 1){
                        if($interval->i == 1)
                                $time_message = $interval->i." minute ago";
                        else
                                $time_message = $interval->i." minutes ago";
                }   
                else {
                        if($interval->s < 30)
                                $time_message = "Just now";
                        else
                                $time_message = $interval->s." seconds ago";
                }  

                //Check if post came from mobile device
                if($mobile_device == "yes")
                    $from_mobile = "&nbsp;&nbsp;&nbsp;Via mobile device";
                else 
                    $from_mobile = "";

                //String containing post
                $str.= "
                        <div class='swirl_post' onClick='javascript:toggle$id()'>
                            <div class='swirl_profile_pic'>
                                <img src='$profile_pic' width='50'>
                            </div>
                            <div class='posted_by' style='color: #ACACAC;'>
                                <a href='$added_by'> $firstName $lastName </a> &nbsp;&nbsp;&nbsp;$time_message $from_mobile
                                $delete_button
                            </div>
                            <div id='post_body'>$body<br/><br/></div>

                            <div class='newsfeedPostOptions'>
                                Comments ($comments_check_num_rows)&nbsp;&nbsp;&nbsp
                                <iframe src='swirl_bump.php?post_id=$id' scrolling='no'> </iframe>
                            </div>
            
                        </div>
                        <div class='swirl_comment' id='toggleComment$id' style='display: none;'>
                            <iframe src='./swirl_comment_frame.php?post_id=$id' frameborder='0' id='comment_iframe'></iframe>
                        </div>
                        <br>
                        <hr style='margin:0';/>
                        <br>
                        ";

                ?>
                <script>
                $(document).ready(function(){
                    $('#post<?php echo $id; ?>').on('click', function(){
                        bootbox.confirm("Are you sure you want to delete this swirl?", function(result) {
                            $.post("includes/form_handlers/delete_swirl.php?post_id=<?php echo $id; ?>",{result:result});

                            if(result)
                                location.reload();
                        }); 
                    });
                });
                </script>
                <?php             

	        }// End while loop 

	        //If posts were loaded
	        if($count > $limit)
	        	//Holds value of next page. Must stay hidden
	        	$str.="<input type='hidden' class='nextpage' value='".($page + 1)."'><input type='hidden' class='noMorePosts' value='false'>";
	        else 
	        	//No more posts to load. Show 'Finished' message
	        	$str .= "<input type='hidden' class='noMorePosts' value='true'><p style='text-align: center;'>No more posts to show!</p>";
	    }
	  
	    //Show swirls
	    echo $str; 
	} //End load posts friend function

}

?>