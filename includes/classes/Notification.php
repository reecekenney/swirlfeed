<?php 
class Notification {
	private $con; //MySQL connection variable 
	private $user_obj; //User object for user logged in

	public function __construct($con, $userLoggedIn) {
		$this->con = $con; //MySQL connection variable 
		$this->user_obj = new User($con, $userLoggedIn); //Username for user logged in
	}

	//Returns number of unread notifications 
	public function getUnreadNumber(){
		$userLoggedIn = $this->user_obj->getUsername(); //Username of user logged in
		$query = $this->con->query("SELECT id FROM notifications WHERE viewed='no' AND user_to='$userLoggedIn'");
		return $query->num_rows;
	}

	public function getNotifications($data, $limit){

		$page = $data['page']; //Page number passed as parameter
		$userLoggedIn = $this->user_obj->getUsername(); //Username of user logged in
		$str = ""; //String to echo at end. Contains all notifications

		if($page == 1){
	        $start = 0;  //Start at first post
	    }
	    else{
	        $start = ($page - 1) * $limit; //Start where last loaded posts left off
	    }

	    //Set viewed to yes for all notifications for that user. 
	    //Viewed mysql column is when user opens notification window
	    //Opened mysql column is when user actually clicks on notification 
	    $set_viewed = $this->con->query("UPDATE notifications SET viewed='yes' WHERE user_to='$userLoggedIn'");

		$data = $this->con->query("SELECT * FROM notifications WHERE user_to='$userLoggedIn' ORDER BY id DESC");

		//If no notifications
		if($data->num_rows == 0){
			echo "You don't have any notifications to load.";
			return;
		} 

		$num_iterations = 0; //Number of results checked (not neccassarily posted)
	    $count = 1; //Number of results posted
		while( $row = $data->fetch_array(MYSQLI_ASSOC) ){

			//If not reached start position from last loads yet
        	if($num_iterations++ < $start)
        		continue;

        	//Once 5 notifications have been loaded, stop
        	if($count > $limit){
        		break;
        	}	
        	else {
        		$count++; //Number of notifications loaded + 1
        	}
        		

			$user_from = $row['user_from'];

			$query = $this->con->query("SELECT * FROM users WHERE username='$user_from'");
			$userData = $query->fetch_array(MYSQLI_ASSOC);


			//Calculate how long ago notification was received
			$date_time_now = date("Y-m-d H:i:s");
            $start_date = new DateTime($row['datetime']);
            $end_date = new DateTime($date_time_now);
            $interval = $start_date->diff($end_date);
            if($interval->y >= 1){
                    if($interval->y == 1)
                            $time_message = $interval->y." year ago";
                    else                
                            $time_message = $interval->y." years ago";
            }                
            else if($interval->m >= 1){
                    if($interval->d == 0)
                            $days = " ago";
                    else if($interval->d == 1)
                            $days = $interval->d." day ago";
                    else
                            $days = $interval->d." days ago";
                    if($interval->m == 1)
                            $time_message = $interval->m." month ".$days;
                    else
                            $time_message = $interval->m." months ".$days;
            }                
            else if($interval->d >= 1){
                    if($interval->d == 1)
                            $time_message = "Yesterday";
                    else
                            $time_message = $interval->d." days ago";
            }                
            else if($interval->h >= 1){
                    if($interval->h == 1)
                            $time_message = $interval->h." hour ago";
                    else
                            $time_message = $interval->h." hours ago";
            } 
            else if($interval->i >= 1){
                    if($interval->i == 1)
                            $time_message = $interval->i." minute ago";
                    else
                            $time_message = $interval->i." minutes ago";
            }   
            else {
                    if($interval->s < 30)
                            $time_message = "Just now";
                    else
                            $time_message = $interval->s." seconds ago";
            }  

            $opened = $row['opened']; //If yes, this notification has been clicked on before.
            $style = ($opened == 'no') ? "background-color: #DDEDFF;" : ""; //If unopened, change background color slightly

			$str .= "<a href='".$row['link']."'>
						<div class='resultDisplay resultDisplayNotification' style='".$style."'>
						
                                <div class='notificationsProfilePic'>
                                    <img src='".$userData['profile_pic']."'>
                                </div>
                                <p class='timestamp_smaller' id='grey'>".$time_message."</p>".$row['message']."
                    	</div>
                    </a>";

		} //End while loop

		//If posts were loaded
	        if($count > $limit)
	        	//Holds value of next page. Must stay hidden
	        	$str.="<input type='hidden' class='nextpageDropdownData' value='".($page + 1)."'><input type='hidden' class='noMoreDropdownData' value='false'>";
	        else 
	        	//No more Notifications to load. Show 'Finished' message
	        	$str .= "<input type='hidden' class='noMoreDropdownData' value='true'><p style='text-align: center;'>No more notifications to load!</p>";

		echo $str;
	}

	//Insert Notification
	public function insertNotification($post_id, $user_to, $type){
		$userLoggedIn = $this->user_obj->getUsername(); //Username of user logged in
		$userLoggedInName = $this->user_obj->getFirstAndLastName(); //First and last name of user logged in

		$date_time = date("Y-m-d H:i:s"); //Current date and time

		switch ($type) {
			case 'swirl_comment':
				$message = $userLoggedInName." commented on a swirl you posted.";
				break;
			case 'swirl_bump':
				$message = $userLoggedInName." bumped a swirl you posted.";
				break;
			case 'profile_post':
				$message = $userLoggedInName." posted on your profile.";
				break;
			case 'swirl_comment_non_owner':
				$message = $userLoggedInName." commented on a swirl you commented on.";
				break;
			case 'swirl_profile_post_comment':
				$message = $userLoggedInName." commented on your profile post.";
				break;
		}

		$link = "swirl.php?id=".$post_id; //Link of swirl to include in <a> tag

		$query = $this->con->query("INSERT INTO notifications VALUES('', '$user_to', '$userLoggedIn', '$message', '$link', '$date_time', 'no', 'no')");
	}
}
?>