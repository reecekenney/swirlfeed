<?php
class User {
	private $user; //Holds array of user details (See constructor)
	private $con; //Mysql connection 

	public function __construct($con, $user) {
		$this->con = $con;
		$user_details_query = mysqli_query($con, "SELECT * FROM users WHERE username='$user'");
		$this->user = mysqli_fetch_array($user_details_query); //Array of user details
	}

	public function getUsername(){
		return $this->user['username'];
	}

	public function getNumberOfFriendRequests(){
		$username = $this->user['username'];
		$requests_query = $this->con->query("SELECT id FROM friend_requests WHERE user_to='$username'");
		return $requests_query->num_rows;
	}

	public function getFirstAndLastName(){
		$username = $this->user['username'];
		$name_query = mysqli_query($this->con, "SELECT first_name, last_name FROM users WHERE username='$username'");
		$row = mysqli_fetch_assoc($name_query);
		return $row['first_name']." ".$row['last_name'];
	}

	public function getFriendArray(){
		$username = $this->user['username'];
		$name_query = mysqli_query($this->con, "SELECT friend_array FROM users WHERE username='$username'");
		$row = mysqli_fetch_assoc($name_query);
		return $row['friend_array'];
	}

	public function getProfilePic(){
		$username = $this->user['username'];
		$name_query = mysqli_query($this->con, "SELECT profile_pic FROM users WHERE username='$username'");
		$row = mysqli_fetch_assoc($name_query);
		return $row['profile_pic'];
	}

	public function isClosed(){
		$username = $this->user['username'];
		$is_closed_query = mysqli_query($this->con, "SELECT user_closed FROM users WHERE username='$username'");
		$row = mysqli_fetch_assoc($is_closed_query);
		return $row['user_closed'];
	}

	//Check if main user (user details from constructor) 
	//is friends with user to check (passed as parameter)
	public function isFriend($username_to_check) {
		//Put comma at either side of the username to check
		//Usernames in friend array always have a comma. 
		//Example friend array: ,username1,username2,username3,
		$usernameComma = ",".$username_to_check.",";
		//Check if username to check appears in friend array
		if((strstr($this->user['friend_array'], $usernameComma)) || ($username_to_check == $this->user['username']))
			return true;
		else 
			return false;
	}

	public function removeFriend($user_to_remove){
		$logged_in_user = $this->user['username']; //Logged in user's username

		//Friend array for user who owns profile
		$add_friend_check_username = mysqli_query($this->con, "SELECT friend_array FROM users WHERE username='$user_to_remove'");
		$row = mysqli_fetch_assoc($add_friend_check_username);
		$friendArrayUsername = $row['friend_array'];

		//Remove other user from logged in user's friend array
		$new_friend_array = str_replace($user_to_remove.",", "", $this->user['friend_array']);
		$removeFriendQuery = mysqli_query($this->con, "UPDATE users SET friend_array='$new_friend_array' WHERE username='$logged_in_user'");

		//Remove logged in user from other persons array
		$new_friend_array = str_replace($this->user['username'].",", "", $friendArrayUsername);
		$removeFriendQuery_username = mysqli_query($this->con, "UPDATE users SET friend_array='$new_friend_array' WHERE username='$user_to_remove'");
	}

	//Sends a request to the user passed as parameter 
	public function sendRequest($user_to){
		$user_from = $this->user['username'];
		$createRequest = mysqli_query($this->con, "INSERT INTO friend_requests VALUES ('', '$user_to', '$user_from')");
	}

	//Returns true if a request has been recieved from the user passed as parameter
	public function didReceiveRequest($user_from) {
		$user_to = $this->user['username'];
		$check_request_query = mysqli_query($this->con, "SELECT * FROM friend_requests WHERE user_to='$user_to' AND user_from='$user_from'");
		if(mysqli_num_rows($check_request_query) != 0)
			return true;
		else 
			return false;
	}

	//Returns true if a request has already been sent to the user passed as parameter (awaiting response)
	public function didSendRequest($user_to) {
		$user_from = $this->user['username'];
		$check_request_query = mysqli_query($this->con, "SELECT * FROM friend_requests WHERE user_to='$user_to' AND user_from='$user_from'");
		if(mysqli_num_rows($check_request_query) != 0)
			return true;
		else 
			return false;
	}

	//Iterates through friend arrays and returns the number of equal elements
	public function getMutualFriends($user_to_check) {
		$mutual_friends = 0; //Stores number ofmutual friends
		$user_array = $this->user['friend_array']; //Friend array (string) of user. From 'users' table
		$user_array_explode = explode(",", $user_array); //Friend array string split into array at commas

		//Get friend array of user to check
		$get_friend_array = mysqli_query($this->con, "SELECT friend_array FROM users WHERE username='$user_to_check'");
		$row = mysqli_fetch_assoc($get_friend_array);
		$user_to_check_array = $row['friend_array']; //Friend array of user to check
		$user_to_check_array_explode = explode(",", $user_to_check_array); //Friend array string split into array at commas

		foreach($user_array_explode as $i){

			foreach($user_to_check_array_explode as $j){
				//If elements are equal AND not empty, increase number of mututal friends 
				if($i == $j && $i != ""){
					$mutual_friends++; //Increase number of mutual friends
				}
			} //End foreach $j
		} //End foreach $i
		return $mutual_friends;

	} //End getMutualfriends
}
?>