<?php 
if(isset($_POST['submit'])){
	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
	$email = $_POST['email'];

	//Check if new email is already registered
	$email_check_query = mysqli_query($con, "SELECT username FROM users WHERE email='$email'");
	$row = mysqli_fetch_assoc($email_check_query);
	$matched_user = $row['username'];

	if($matched_user == "" || $matched_user == $userLoggedIn){
		$message = "Details updated!<br><br>";

		$query = mysqli_query($con, "UPDATE users SET first_name='$first_name', last_name='$last_name', email='$email'WHERE username='$userLoggedIn'");
	}
	else 
		$message = "That email is already in use!<br><br>";
}
else {
	$message = "";
}

if(isset($_POST['close_account'])){
	header("Location: close_account.php");
}

//Change password
if(isset($_POST['submit_password'])) {
	//Password variables
	$oldPassword = strip_tags($_POST['old_password']);
	$newPassword = strip_tags($_POST['new_password_1']);
	$repeatPassword = strip_tags($_POST['new_password_2']);

	$passwordQuery = mysqli_query($con, "SELECT password FROM users WHERE username='$userLoggedIn'");
	$row = mysqli_fetch_assoc($passwordQuery);
	$dbPassword = $row['password'];

	//MD5 the old password before we check if it matches 
	$oldPasswordMD5 = md5($oldPassword);

	//Check whether old password equals $dpPassword
	if($oldPasswordMD5 == $dbPassword) {
		//Continue changing the users password
		//Check whether the 2 new passwords match
		if($newPassword == $repeatPassword) {
			if(strlen($newPassword) <= 4) {
				$password_message = "Sorry, your password must be at least 5 characters long!<br><br>";
			}
			else {
				//MD5 new password before updating in database
				$newPasswordMD5 = md5($newPassword);
				//Update the users password in the database
				$passwordUpdateQuery = mysqli_query($con, "UPDATE users SET password='$newPasswordMD5' WHERE username='$userLoggedIn'");
				$password_message = "Password has been changed!<br><br>";
			}
		}
		else {
			$password_message = "Your two new passwords don't match!<br><br>";
		}
	}
	else {
		$password_message = "The old password is incorrect!<br><br>";
	}
}
else {
	$password_message = "";
}
 ?>