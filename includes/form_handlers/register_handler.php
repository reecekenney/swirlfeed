<?//declaring variables to prevent errors
$fname = ""; //First Name
$lname = ""; //Last Name
$em = ""; //Email
$em2 = ""; //Email2
$password = ""; //Password
$password2 = ""; //Password2
$date = ""; //Sign up date
$eCheck = ""; //Check if email exists
$error_array = array(); //Array to hold any error messages due to incorrect login details

$reg = @$_POST['register_button'];
if($reg) {
	//Registration form values
	$fname = strip_tags($_POST['reg_fname']); //Get first name
	$fname = str_replace(' ', '', $fname); //Remove spaces
	$fname = ucfirst(strtolower($fname)); //Capitalize first letter. Make all other letters lowercase
	$_SESSION['reg_fname'] = $fname; //Store first name into session variable 

	$lname = strip_tags($_POST['reg_lname']); //Get last name
	$lname = ucfirst(strtolower($lname)); //Capitalize first letter. Make all other letters lowercase
	$lname = str_replace(' ', '', $lname); //Remove spaces
	$_SESSION['reg_lname'] = $lname; //Store last name into session variable

	$em = strtolower(strip_tags($_POST['reg_email'])); //Get email
	$em = str_replace(' ', '', $em); //Remove spaces
	$_SESSION['reg_email'] = $em; //Store email in session variable

	$em2 = strtolower(strip_tags($_POST['reg_email2'])); //Get email2 
	$em2 = str_replace(' ', '', $em2); //Remove spaces
	$_SESSION['reg_email2'] = $em2; //Store email2 in session variable

	$password = strip_tags($_POST['reg_password']); //Get password
	$password2 = strip_tags($_POST['reg_password2']); //Get password2

	$date = date("Y-m-d"); //Current date
	
	if($em == $em2) {
		//Check if email is valid format
		if (filter_var($em, FILTER_VALIDATE_EMAIL))
		{
		    // Sanitize the e-mail
		    $em = filter_var($em, FILTER_SANITIZE_EMAIL);

		    //Check if email already exists
		    if(email_exists($em, $con)) 
				array_push($error_array, "This email address has already been registered to an account!<br>");
		}
		else {
			array_push($error_array, "Invalid email format!<br>");
		}	
	}
	else {
		array_push($error_array, "Your E-mails do not match!<br>");
	}
	
	//Check all of the fields have been submitted
	if(!$fname || !$lname || !$em || !$em2 || !$password || !$password2) {
		array_push($error_array, "Please fill in all the fields!<br>");
	}

	if(strlen($fname) > 25 || strlen($fname) < 2){
		array_push($error_array, "Your first name must be between 2 and 25 characters!<br>");
	}
				
	if(strlen($lname) > 25 || strlen($lname) < 2){
		array_push($error_array, "Your last name must be between 2 and 25 characters!<br>");
	}

	if($password != $password2) {
		array_push($error_array, "Your passwords do not match!<br>");
	}
	else {
		if (preg_match('/[^A-Za-z0-9]/', $password)){
			array_push($error_array, "Your password can only contain english letters and numbers!<br>");
		}
	}
	if(strlen($password) > 30 || strlen($password) < 5) {
		array_push($error_array, "Your password must be between 5 and 30 characters long!<br>");
	}
	//$_SESSION['error_array'] = $error_array;
	//If array is empty, there were no errors. Enter details into database.
	if(empty($error_array)){
		$password = md5($password); //encrypt password before sending to database

		//Generate username by concatenating first and last names
		$username = strtolower($fname."_".$lname);
		$check_username_query = mysqli_query($con, "SELECT username FROM users WHERE username='$username'");
		$i = 0;
		//If username already exists, add number to end and check again
		while(mysqli_num_rows($check_username_query) != 0){
			$i++;
			$username = $username."_".$i;
			$check_username_query = mysqli_query($con, "SELECT username FROM users WHERE username='$username'");
		}

		//Generate random number to detirmine default profile picture colour 
		$random_num = rand(1, 16); //Random numberbetween 1 and 3
		switch ($random_num) {
			case '1':
				$profile_pic = "assets/images/profile_pics/defaults/head_alizarin.png";
				break;
			case '2':
				$profile_pic = "assets/images/profile_pics/defaults/head_amethyst.png";
				break;
			case '3':
				$profile_pic = "assets/images/profile_pics/defaults/head_belize_hole.png";
				break;
			case '4':
				$profile_pic = "assets/images/profile_pics/defaults/head_carrot.png";
				break;
			case '5':
				$profile_pic = "assets/images/profile_pics/defaults/head_deep_blue.png";
				break;
			case '6':
				$profile_pic = "assets/images/profile_pics/defaults/head_emerald.png";
				break;
			case '7':
				$profile_pic = "assets/images/profile_pics/defaults/head_green_sea.png";
				break;
			case '8':
				$profile_pic = "assets/images/profile_pics/defaults/head_nephritis.png";
				break;
			case '9':
				$profile_pic = "assets/images/profile_pics/defaults/head_pete_river.png";
				break;
			case '10':
				$profile_pic = "assets/images/profile_pics/defaults/head_pomegranate.png";
				break;
			case '11':
				$profile_pic = "assets/images/profile_pics/defaults/head_pumpkin.png";
				break;
			case '12':
				$profile_pic = "assets/images/profile_pics/defaults/head_red.png";
				break;
			case '13':
				$profile_pic = "assets/images/profile_pics/defaults/head_sun_flower.png";
				break;
			case '14':
				$profile_pic = "assets/images/profile_pics/defaults/head_turqoise.png";
				break;
			case '15':
				$profile_pic = "assets/images/profile_pics/defaults/head_wet_asphalt.png";
				break;
			case '16':
				$profile_pic = "assets/images/profile_pics/defaults/head_wisteria.png";
				break;
		}

		$query = mysqli_query($con, "INSERT INTO `users` VALUES ('', '$fname', '$lname', '$username', '$em', '$password', '$date', '$profile_pic', '0', '0', 'no', ',')");

		array_push($error_array, "<span style='color: #14C800'>You're all set! Go ahead and login!</span><br>");
		//Clear session variables
		$_SESSION['reg_fname'] = "";
		$_SESSION['reg_lname'] = "";
		$_SESSION['reg_email'] = "";
		$_SESSION['reg_email2'] = "";
		
	}
				
}?>