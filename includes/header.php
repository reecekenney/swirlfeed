<?php 
include("./config/config.php");
include("./includes/classes/User.php");
include("./includes/classes/Message.php");
include("./includes/classes/Swirl.php");
include("./includes/classes/Notification.php");

if(isset($_SESSION["username"])){
	$userLoggedIn = $_SESSION["username"];
	$user_details_query = mysqli_query($con, "SELECT * FROM users WHERE username='$userLoggedIn'");
	$user = mysqli_fetch_array($user_details_query);
}
else {
	header("Location: register.php");
}

//Check what device user is using
$iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
$palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
$berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");

?>
<html>
<head>
	<title>Swirlfeed</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css"/> 
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="assets/js/jquery.Jcrop.js"></script>
	<link rel="stylesheet" href="assets/css/jquery.Jcrop.css" type="text/css" />
	<link rel="stylesheet" href="assets/js/jquery.paulund_modal_box.js"/>
	<script src="assets/bootstrap/js/bootstrap.js"></script>
	<script src="assets/js/jcrop_bits.js"></script>
	<script src="assets/js/swirlfeed.js"></script>
	<script src="assets/js/bootbox.min.js"></script>

</head>
<body>
	<div class="top_bar">
			<div class="logo">
				<a href="index.php">Swirlfeed!</a>
			</div>

			<div class="search">
				<form action="search.php" method="GET" name="search_form">
					<input type="text" onkeyup="getLiveSearchUsers(this.value, '<?php echo $userLoggedIn; ?>')" name="q" placeholder='Search...' autocomplete='off' id='search-text-input'>
					<div class='button_holder'>
					    <img src='assets/images/icons/magnifying_glass.png'/>
					</div>
				</form>
				<div class="search_results">
				</div>
				<div class="search_results_footer_empty">
				</div>
			</div>

			
			<nav>
				<?php 
					//Get number of unread notifications
					$notification = new Notification($con, $userLoggedIn);
					$num_notifications = $notification->getUnreadNumber(); //Returns number of unread notifications

					//Get number of unread messages
					$messages = new Message($con, $userLoggedIn);
					$num_messages = $messages->getUnreadNumber(); //Returns number of unread messages

					//Get number of unread friend requests
					$user_obj = new User($con, $userLoggedIn);
					$num_friend_requests = $user_obj->getNumberOfFriendRequests(); //Returns number of unread friend requests
				?>
				<a href="<?php echo $user['username']; ?>">
					<?php echo $user['first_name']; ?>
				</a>
				<a href="javascript:void(0);" onclick="getDropdownData('<?php echo $userLoggedIn; ?>', 'message')">
					<i class="icon fa  fa-envelope-o fa-lg"></i>
					<?php 
					if($num_messages > 0)
						echo '<span class="notification_badge" id="unread_message">'.$num_messages.'</span>';
					?>
				</a>
				<a href="index.php">
					<i class="icon fa  fa-home fa-lg"></i>
				</a>
				<a href="javascript:void(0);" onclick="getDropdownData('<?php echo $userLoggedIn; ?>', 'notification')"> 
					<i class="icon fa  fa-bell-o fa-lg"></i>
					<?php 
					if($num_notifications > 0)
						echo '<span class="notification_badge" id="unread_notification">'.$num_notifications.'</span>';
					?>
				</a>
				<a href="requests.php">
					<i class="icon fa  fa-users fa-lg"></i>
					<?php 
					if($num_friend_requests > 0)
						echo '<span class="notification_badge" id="unread_request">'.$num_friend_requests.'</span>';
					?>
				</a>
				<a href="settings.php">
					<i class="icon fa  fa-cog fa-lg"></i>
				</a>
				<a href="includes/handlers/logout.php">
					<i class="icon fa  fa-sign-out fa-lg"></i>
				</a>
			</nav>
			<!-- Holds drop down data e.g. notifications, messages -->
			<div class="dropdown_data_window">	
			</div>
			<input type="hidden" id="dropdown_data_type" value=""><!-- Value is used for the type of data to load e.g. messages, notifications -->
			
		</div>

		<!-- script for loading dropdown_data AND infinite scrolling -->
		<script>
			var userLoggedIn = '<?php echo $userLoggedIn; ?>';

			$(document).ready(function() {

				

				$(window).scroll(function() {

					var inner_height = $('.dropdown_data_window').innerHeight(); //Get height of div containing dropdown data
				    var scroll_top = $(".dropdown_data_window").scrollTop();
				    var page = $('.dropdown_data_window').find('.nextpageDropdownData').val();
				    var noMoreData = $('.dropdown_data_window').find('.noMoreDropdownData').val();

					if ((scroll_top + inner_height >= $('.dropdown_data_window')[0].scrollHeight) && noMoreData == 'false') {
				        //$('#loading_dropdown_window').show(); //Show loading icon

				    	var pageName; //Hold name of page to send ajax request to
				    	var type = $('#dropdown_data_type').val(); //Value in the hidden input field in dropdown_data_window div. Hold type of data to load.

				        if(type == 'notification')
				          pageName = "ajax_load_notifications.php"; //Page to load notifications
				        else if(type == 'message')
				          pageName = "ajax_load_messages.php"; //Page to load messages
				        else if(type == 'friend_request')
				          pageName = "ajax_load_friend_requests.php"; //Page to load friend requests 

					    var ajaxreq = $.ajax({
						    url:"includes/handlers/" + pageName,
					        type:"POST",
					        data:"page=" + page + "&userLoggedIn=" + userLoggedIn, //Function to call (functionName) and page number
					        cache: false,

					        success: function(response){

								$('.dropdown_data_window').find('.nextpageDropdownData').remove(); //Remove current .nextPage (hidden input)
							    $('.dropdown_data_window').find('.noMoreDropdownData').remove(); //Remove current .noMorePosts (hidden input)
							    $('#loading_dropdown_window').hide(); //Hide loading icon 
							   
							    $('.dropdown_data_window').append(response); //Append with new posts 
							}
						
					    });
				    }

				    return false;
				});
			}); //End document.ready
		</script>

	<div class="wrapper">